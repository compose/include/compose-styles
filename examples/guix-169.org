#+LaTeX_CLASS: beamer
#+LATEX_CLASS_OPTIONS: [aspectratio=169]
#+LANGUAGE: en
#+OPTIONS: H:2 toc:nil author:nil email:nil date:nil ^:{}
#+BEAMER_FRAME_LEVEL: 1
#+TITLE: Guix 16:9 theme example
#+LaTeX_HEADER: \author{
#+LaTeX_HEADER:   Emmanuel Agullo \\ [-1ex]
#+LaTeX_HEADER:   \texttt{myfirstname.myname@inria.fr}
#+LaTeX_HEADER: }
#+BEAMER_HEADER: \usepackage{helvet}

#+BEAMER_HEADER: \usepackage[orgmode, inria]{beamerthemeguix}
# The following three lines refine the beamer theme guix so that it relaxed the aspect ratio constraints to cope with the 16:9 above definitition.
#+BEAMER_HEADER: \setbeamertemplate{background}{
#+BEAMER_HEADER:   \includegraphics[width=\paperwidth]{artwork/background}
#+BEAMER_HEADER: }

#+BEAMER_HEADER: \institute{Inria Bordeaux Sud-Ouest}
#+BEAMER_HEADER: \date[Nov 5, 2024]{
#+BEAMER_HEADER:   Guix theme example \\
#+BEAMER_HEADER:   Fromulary 32, 3022
#+BEAMER_HEADER: }

* My first part

** My first slide

Hello!
