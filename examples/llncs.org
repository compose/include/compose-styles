#+TITLE: LLNCS theme example
#+AUTHOR: Marek Felšöci
#+OPTIONS: toc:nil ^:{} author:nil
#+LaTeX_CLASS: llncs
#+LaTeX_CLASS_OPTIONS: [runningheads]
#+LaTeX_HEADER: \title{
#+LaTeX_HEADER:   LLNCS theme example
#+LaTeX_HEADER: }
#+LaTeX_HEADER: \titlerunning{
#+LaTeX_HEADER:   LLNCS theme example
#+LaTeX_HEADER: }
#+LaTeX_HEADER: \author{
#+LaTeX_HEADER:   Marek Felšöci\inst{1}
#+LaTeX_HEADER: }  
#+LaTeX_HEADER: \authorrunning{ 
#+LaTeX_HEADER:   Marek Felšöci
#+LaTeX_HEADER: }
#+LaTeX_HEADER: \institute{
#+LaTeX_HEADER:   Inria Bordeaux Sud-Ouest, Talence, France \\
#+LaTeX_HEADER:   \email{
#+LaTeX_HEADER:     \{first.last\}@inria.fr
#+LaTeX_HEADER:   }
#+LaTeX_HEADER: }

#+BEGIN_EXPORT LaTeX
\begin{abstract}
  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas sit amet
  rhoncus est, vitae fermentum velit. Fusce faucibus lorem eget ullamcorper
  porttitor. Morbi aliquam augue et euismod ultrices. Sed sed euismod metus, eu
  imperdiet tortor. Sed fermentum, ante vel feugiat tristique, purus turpis
  volutpat nibh, pulvinar ultricies justo ligula nec nunc. Vestibulum ante ipsum
  primis in faucibus orci luctus et ultrices posuere cubilia curae; Cras
  tincidunt eleifend posuere. Proin nec porta ante. Maecenas sem mi, dignissim
  eu molestie ac, semper vel quam.
  \keywords{
    lorem \and ipsum
  }
\end{abstract}
#+END_EXPORT

#+INCLUDE: "./lorem.org"

