# compose-styles: LaTeX and Beamer styles

Useful LaTeX styles, Beamer and poster templates.

The instructions below assume a clone of this repository in the same location as
the target LaTeX doucment.

## Conference papers

This repository provides LaTeX styles for Compas, LLNCS and SIAM conference
papers in the `compas`, `llncs` and `siam` folders respectively. To use one of
the styles, ensure the corresponding sources files (`*.sty` and `*.bst`) are
placed in the same location as the target LaTeX document during export.

See examples of usage in `examples/compas.org`, `examples/llncs.org` and
`examples/siam.org`, respectively.

## Inria reports

The `RR` folder contains a LaTeX package for publishing research and technical
reports with the corporate identity of Inria. To use this template, include the
following line into the header of the target LaTeX document:

```latex
\usepackage{compose-styles/RR/RR}
```

The package provides several options:
- `french` prints the title page in French instead of English,
- `longabstract` creates a separate page for English abstract if it is too long
  to fit on the second page,
- `sfont` and `ssfont` allow for changing respectively the serif and the
  sans-serif font families used to typeset the opening and the closing of the
  document.
  
See `examples/RR.org` for an example of how to set up a document with this
template.
  
## kbordermatrix

This LaTeX style allows for typesetting anotated matrices. It has two variants.

### `kbordermatrix.sty`

This variant uses minimal spacing between matrix elements to save as much space
as possible.

![kbordermatrix](kbordermatrix/kbordermatrix.png)

To use this style, include the corresponding `\usepackage` directive into the
target LaTeX document, like so:

```latex
% Without the *.sty extension!
\usepackage{compose-styles/kbordermatrix/kbordermatrix}
```

### `kbordermatrix-spaced.sty`

This variant uses larger spacing between matrix elements which may appear more
visually appealing.

![kbordermatrix-spaced](kbordermatrix/kbordermatrix-spaced.png)

To use this style, include the corresponding `\usepackage` directive into the
target LaTeX document, like so:

```latex
% Without the *.sty extension!
\usepackage{compose-styles/kbordermatrix/kbordermatrix-spaced}
```

## IEEEoverride

The `IEEEtran` document class provided within the Texlive distribution has an
option to produce anonymized documents for peer-reviewing. However, the first
page of the output still features author names. The source code block in the
`IEEEoverride.tex` file prevents this page from being published. To use this
workaround code, simply include the following line in the preamble of the target
LaTeX document:

```latex
\input{compose-styles/IEEEoverride/IEEEoverride.tex}
```

## beamerthemeguix

To talk about a Guix-related subject, you may want to use a dedicated Beamer
template for your slides. To apply the `beamerthemeguix.sty` style, include the
corresponding `\usepackage` directive into the target LaTeX document, like so:

```latex
% Without the *.sty extension!
\usepackage{compose-styles/beamerthemeguix/beamerthemeguix}
```

The `beamerthemeguix` template provides the `inria` and the `orgmode` options to
include respecitvely the Inria and the Org mode logos in the footer of the
slides.

See `examples/guix.org` for an example of how to set up a document with this
template.

## beamerthemeinria

If you are looking for a simple Inria Beamer theme, have a look at the
`beamerthemeinria.sty` style. To use it, put the corresponding `\usepackage`
directive into the target LaTeX document, like so:

```latex
% Without the *.sty extension!
\usepackage{compose-styles/beamerthemeinria/beamerthemeinria}
```

See `examples/inria.org` for an example of how to set up a document with this
template.

## Poster

Finally, the `poster` folder provides a LaTeX document template for posters.
See `examples/poster.org` for an example.
